package controllers

import (
	"fmt"
	"github.com/gin-gonic/gin"
	"net/http"
	"testing/src/api/services"
)

func GetCountry(c *gin.Context) {
	country, err := services.LocationsService.GetCountry(c.Param("country_id"))
	if err != nil {
		c.JSON(err.Status, err)
		return
	}
	c.JSON(http.StatusOK, country)
}
func GetHello(c *gin.Context) {
	fmt.Println("We touched the function")
	//country, err := services.GetCountry(c.Param("country_id"))
	//if err != nil {
	//	c.JSON(err.Status, err)
	//	return
	//}
	c.String(http.StatusOK, "Hello sir")
}