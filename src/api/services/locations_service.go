package services

import (
	"testing/src/api/domain/locations"
	"testing/src/api/provider/locations_provider"
	"testing/src/api/utils/errors"
)

type locationsService struct {}

type locationServiceInterface interface {
	GetCountry(countryId string) (*locations.Country, *errors.ApiError)
}

var (
	LocationsService locationServiceInterface
)
func init(){
	LocationsService = &locationsService{}
}

//For we to mock the provider, in the provider package, we are going to have the above trick that we used to mock the service in order to achieve unit test for the controller
//note, from the trick above=, the struct is now implementing the "locationServiceInterface" beccause, it is defining a method the interface has "GetCountry"
func (s *locationsService) GetCountry(countryId string) (*locations.Country, *errors.ApiError) {
	return locations_provider.GetCountry(countryId)
}