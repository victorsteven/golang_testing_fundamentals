package services

import (
	"github.com/stretchr/testify/assert"
	"testing"
	"testing/src/api/utils/sort"
)

//int tests

func TestBubbleSort(t *testing.T){
	//elements := []int{0,4,6,3,2,7}
	elements := sort.GetElements(10)
	Sort(elements)
	//ans := []int{7,6,4,3,2,0}
	assert.EqualValues(t, elements[len(elements)-1], 9)
}

func TestSort(t *testing.T){
	//elements := []int{0,4,6,3,2,7}
	elements := sort.GetElements(10001)
	Sort(elements)
	//ans := []int{7,6,4,3,2,0}
	assert.EqualValues(t, elements[len(elements)-1], 10000)
}
//
//func BenchmarkBubbleSort(b *testing.B) {
//	//elements := []int{0,4,6,3,2,7}
//	elements := sort.GetElements(100000)
//	for i := 0; i< b.N; i++ {
//		BubbleSort(elements)
//	}
//}

func BenchmarkSort10k(b *testing.B) {
	//elements := []int{0,4,6,3,2,7}
	elements := sort.GetElements(20000)
	for i := 0; i< b.N; i++ {
		Sort(elements)
	}
}
func BenchmarkSort100k(b *testing.B) {
	//elements := []int{0,4,6,3,2,7}
	elements := sort.GetElements(100000)
	for i := 0; i< b.N; i++ {
		Sort(elements)
	}
}