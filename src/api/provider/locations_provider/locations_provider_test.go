package locations_provider

import (
	"github.com/mercadolibre/golang-restclient/rest"
	"github.com/stretchr/testify/assert"
	"net/http"
	"testing"
)

func TestGetCountryRestClientError(t *testing.T) {
	rest.StartMockupServer()
	//Execution
	country, err := GetCountry("AR")
	//Validation
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, "invalid restclient error when getting country AR", err.Message)
}

func TestGetCountryNotFound(t *testing.T) {
	rest.StartMockupServer()
	//Execution
	country, err := GetCountry("AR")
	//Validation
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusNotFound, err.Status)
	assert.EqualValues(t, "Country not found", err.Message)
}

//When the API change some parameters. For instance, changes from integer to string in one of the fields
func TestGetCountryInvalidErrorInterface(t *testing.T) {
	rest.StartMockupServer()
	//Execution
	country, err := GetCountry("AR")
	//Validation
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, "invalid error interface when getting country AR", err.Message)
}

func TestGetCountryInvalidJsonResponse(t *testing.T) {
	rest.StartMockupServer()
	//Execution
	country, err := GetCountry("AR")
	//Validation
	assert.Nil(t, country)
	assert.NotNil(t, err)
	assert.EqualValues(t, http.StatusInternalServerError, err.Status)
	assert.EqualValues(t, "error when trying to unmarshal country data for AR", err.Message)
}

func TestGetCountryNoError(t *testing.T) {
	rest.StartMockupServer()
	//Execution
	country, err := GetCountry("AR")
	//Validation
	assert.NotNil(t, country)
	assert.Nil(t, err)
	assert.EqualValues(t, "AR", country.Id)
	assert.EqualValues(t, "GMT-03:00", country.TimeZone)
	assert.EqualValues(t, 24, len(country.States))
}

