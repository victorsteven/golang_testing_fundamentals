package sort

import (
	"github.com/stretchr/testify/assert"
	"testing"
)

func TestBubbleSort(t *testing.T) {
	elements := []int{0,4,6,3,2,7}
	BubbleSort(elements)
	//ans := []int{7,6,4,3,2,0}
	ans := []int{0,2,3,4,6,7}
	assert.EqualValues(t, elements, ans)
}
func TestBubbleSortAlreadySorted(t *testing.T) {
	elements := []int{5,4,3,2,1}

	BubbleSort(elements)

	//ans := []int{7,6,4,3,2,0}
	//assert.EqualValues(t, elements, ans)
}

func TestSort(t *testing.T) {
	elements := []int{0,4,6,3,2,7}
	Sort(elements)
	//ans := []int{7,6,4,3,2,0}
	ans := []int{0,2,3,4,6,7}
	assert.EqualValues(t, elements, ans)
}



func BenchmarkBubbleSort(b *testing.B) {
	//elements := []int{0,4,6,3,2,7}
	elements := GetElements(100000)
	for i := 0; i< b.N; i++ {
		BubbleSort(elements)
	}
}

func BenchmarkSort(b *testing.B) {
	//elements := []int{0,4,6,3,2,7}
	elements := GetElements(100000)
	for i := 0; i< b.N; i++ {
		Sort(elements)
	}
}
