package app

import "testing/src/api/controllers"

func mapUrls(){
	router.GET("/locations/countries/:country_id", controllers.GetCountry)
	router.GET("/", controllers.GetHello)

}