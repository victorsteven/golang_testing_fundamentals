module testing

go 1.13

require (
	github.com/gin-gonic/gin v1.4.0
	github.com/mercadolibre/golang-restclient v0.0.0-20170701022150-51958130a0a0
	github.com/stretchr/testify v1.4.0
)
